# Developers guide

Before You start to modify any code in project please review thru this document.

## 1. Project structure

```
    .
    ├── .ci                 # Gitlab pipelines jobs definition
    ├── bin                 # Docker shortcuts
    ├── coverage *          # Coverage report
    ├── dist *              # Production application build
    ├── docker              # Docker images definitions
    ├── docs                # Docs for project
    ├── node_modules *      # Installed packages
    ├── public              # Public assets to include into build that aren't changing
    ├── src                 # Application files
    └── tests               # Mockups and helpers for jest tests
```

Paths with `*` are optional, they're not commited to git, and can appear after execution of some commends.
In root of the app we have also bunch of configuration files, for linter, jest, pipelines, docker and vue.

## 2. Application structure (src path)

```
    .
    ├── __tests__           # Jest test cases are here, this path can occur at any other location
    ├── assets              # CSS + other thing to public build
    ├── components          # Reusable app components
    ├── config              # App config files (not enviroment dependent)
    ├── exceptions          # App custom exceptions
    ├── helpers             # Special helpers classes
    ├── layout              # Main app components
    ├── services            # Classes and types fr all API requests
    ├── store               # Vuex store definitions
    ├── translations        # JSON translations files
    ├── types               # Types definitions for project
    ├── views               # App bigger components
    └── App.vue             # Main vue app container
```

Other files are type definitions and app initialization parts like routing, translations, reusable global components etc.
