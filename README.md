[![pipeline status](https://gitlab.com/ladamczyk/vue-app-skeleton/badges/master/pipeline.svg)](https://gitlab.com/ladamczyk/vue-app-skeleton/-/commits/master)
[![coverage report](https://gitlab.com/ladamczyk/vue-app-skeleton/badges/master/coverage.svg?job=test)](https://gitlab.com/ladamczyk/vue-app-skeleton/-/commits/master?job=test)

## Pre-requirements
- Linux OS (or any other that handle docker + docker compose properly)
- The Latest Docker, for Linux You can use [this tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04), steps 1-2 are mandatory
- The Latest Docker Compose, for Linux You can use [this tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04), steps 1 is mandatory

## 1. How to run project locally:

1. First of all You need to create `.env.local` file based on `.env.local.dist`.
2. Then open terminal and go to project path, we need to build containers:

    ```
    docker-compose build --no-cache
    ```

3. Then we need to install all dependencies:

    ```
    bin/yarn install
    ```

4. And run the project:

    ```
    docker-compose up -d
    ```

5. App will start at:

   [http://localhost:8888](http://localhost:8888)

   The `docker-compose logs -f app` part in step four allow You to see webpack build log.

To run any command on docker container we use:

```
bin/shell [command] [options]
```

We defined shortcut directly to yarn:

```
bin/yarn [command] [options]
```

Most important packages defined in package.json, scripts section:
* `bin/yarn serve` - to run project at docker container, its triggers automatically on server start
* `bin/yarn build` - to build a production package
* `bin/yarn lint` - to execute code linter (eslint)
* `bin/yarn test:unit` - to run tests
* `bin/yarn test:coverage` - to run tests with coverage report

Deployment is automatically done via gitlab pipeline and pm2 from master branch.

Next step is to check [developers guide](./docs/DEVELOPERS_GUIDE.md).