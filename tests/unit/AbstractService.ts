import { AxiosResponse } from 'axios';
import { ReasonPhrases, StatusCodes } from 'http-status-codes';

interface ServiceResponseInterface {
	headers: string[];
	config: any;
	data: any;
}

const serviceResponse: ServiceResponseInterface = {
	headers: [],
	config: {},
	data: {},
};

export const HTTP_OK: AxiosResponse = {
	...serviceResponse,
	status: StatusCodes.OK,
	statusText: ReasonPhrases.OK,
};
