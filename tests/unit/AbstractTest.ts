import { createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import VuexStore from '@/store';
import i18n from '@/i18n';
import wait from '@/wait';

const localVue = createLocalVue();

localVue.use(Vuex);

export const options = {
	store: VuexStore,
	localVue,
	i18n,
	wait,
};
