import Vue from 'vue';
import VueWait from 'vue-wait/src/types/index.d';

declare module 'vue/types/vue' {
	interface Vue {
		$wait: VueWait;
	}
}

declare module 'vue/types/options' {
	/* eslint-disable-next-line @typescript-eslint/no-unused-vars */
	interface ComponentOptions<V extends Vue> {
		wait?: VueWait;
	}
}
