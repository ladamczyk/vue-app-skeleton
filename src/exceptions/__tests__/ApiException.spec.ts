import ApiException from '../ApiException';

describe('ApiException.ts', () => {
	test('Simple 404', () => {
		const exception = () => {
			throw new ApiException({
				status: 404,
				statusText: 'Not found',
			});
		};

		expect(exception).toThrow(ApiException);

		try {
			exception();
			// Fail test if above expression doesn't throw anything.
			expect(true).toBe(false);
		} catch (e) {
			expect(e.status).toBe(404);
			expect(e.message).toBe('Not found');
		}
	});
});
