import { Module } from 'vuex';
import { actions } from './actions';
import { getters } from './getters';
import { mutations } from './mutations';
import { AuthState } from './types';
import { RootState } from '../types';

export const state: AuthState = {
	apiToken: (localStorage && localStorage.getItem('access_token')) || '',
	user: null,
};

export const auth: Module<AuthState, RootState> = {
	state,
	actions,
	mutations,
	getters,
};
