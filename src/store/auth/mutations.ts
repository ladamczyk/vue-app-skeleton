import { MutationTree } from 'vuex';
import { UserInterface } from '@/types/services/Auth';
import { AuthState } from './types';
import { StoreMutations } from '../constants';

export const mutations: MutationTree<AuthState> = {
	[StoreMutations.SET_API_TOKEN]: (state, token: string) => {
		if (localStorage) {
			localStorage.setItem('access_token', token);
			state.apiToken = token;
		}
	},
	[StoreMutations.AUTH_SET_USER]: (state, user: UserInterface) => {
		state.user = user;
	},
};
