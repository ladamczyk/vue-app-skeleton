import { GetterTree } from 'vuex';
import { UserInterface } from '@/types/services/Auth';
import { AuthState } from './types';
import { RootState } from '../types';
import { StoreGetters } from '../constants';

export const getters: GetterTree<AuthState, RootState> = {
	[StoreGetters.GET_API_TOKEN]: (state): string => state.apiToken,
	[StoreGetters.AUTH_GET_USER]: (state): UserInterface => state.user,
};
