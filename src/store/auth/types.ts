import { UserInterface } from '@/types/services/Auth';

export interface AuthState {
	apiToken: string;
	user?: UserInterface;
}
