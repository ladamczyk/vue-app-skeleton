import { MessageInterface } from '@/types';

export interface RootState {
	language: string;
	messages: MessageInterface[];
}
