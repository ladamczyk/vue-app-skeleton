import Vue from 'vue';
import Router from 'vue-router';
import { StoreGetters } from '@/store/constants';
import EventBus from '@/eventBus';
import $store from './store';
import $wait from './wait';

if (process.env.NODE_ENV !== 'test') {
	Vue.use(Router);
}

export enum Route {
	HOME = 'HOME',
	PROTECTED = 'PROTECTED',
	PROTECTED_SAMPLE = 'PROTECTED_SAMPLE',

	NOT_FOUND = 'NOT_FOUND',
}

const router = new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			name: Route.HOME,
			component: () => import('./views/Home.vue'),
		},
		{
			path: '',
			name: Route.PROTECTED,
			component: () => import('./views/Protected.vue'),
			children: [
				{
					path: '/sample',
					name: Route.PROTECTED_SAMPLE,
					component: () => import('./views/protected/Sample.vue'),
				},
			],
		},
		{
			path: '*',
			name: Route.NOT_FOUND,
			component: () => import('./views/404.vue'),
		},
	],
});

// This callback runs before every route change, including on page load.
router.beforeEach((to, from, next) => {
	$wait.start('loading.route');

	if (to.name.includes(Route.PROTECTED) && !$store.getters[StoreGetters.AUTH_GET_USER]) {
		EventBus.$emit('show-login-modal');
	}

	return next();
});

router.afterEach(() => {
	$wait.end('loading.route');
});

export default router;
