import { register } from 'register-service-worker';
import SWUpdateEvent from '@/helpers/SWUpdateEvent';
import EventBus from './eventBus';

if (process.env.NODE_ENV !== 'test') {
	register('service-worker.js', {
		ready(registration) {
			EventBus.$emit('sw-ready', registration);
		},
		cached(registration) {
			EventBus.$emit('sw-cached', new SWUpdateEvent(registration));
		},
		updated(registration) {
			EventBus.$emit('sw-updated', new SWUpdateEvent(registration));
		},
	});
}
