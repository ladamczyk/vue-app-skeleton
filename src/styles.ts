import {
	LayoutPlugin,
	ButtonPlugin,
	NavbarPlugin,
	SpinnerPlugin,
	AlertPlugin,
	TableLitePlugin,
	LinkPlugin,
	ListGroupPlugin,
	ToastPlugin,
	ModalPlugin,
	FormPlugin,
	FormInputPlugin,
	IconsPlugin,
	DropdownPlugin,
	FormGroupPlugin,
} from 'bootstrap-vue';

import '@/assets/sass/index.scss';

class AppStyles {
	public install(Vue: any) {
		// bootstrap
		Vue.use(LayoutPlugin);
		Vue.use(ButtonPlugin);
		Vue.use(NavbarPlugin);
		Vue.use(SpinnerPlugin);
		Vue.use(AlertPlugin);
		Vue.use(TableLitePlugin);
		Vue.use(LinkPlugin);
		Vue.use(ListGroupPlugin);
		Vue.use(ToastPlugin);
		Vue.use(ModalPlugin);
		Vue.use(FormPlugin);
		Vue.use(FormInputPlugin);
		Vue.use(FormGroupPlugin);
		Vue.use(DropdownPlugin);
		Vue.use(IconsPlugin);
	}
}

export default new AppStyles();
