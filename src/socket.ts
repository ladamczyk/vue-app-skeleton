import VueSocketIO from 'vue-socket.io';

/* eslint-disable-next-line import/no-extraneous-dependencies */
import io from 'socket.io-client';

class AppSocket {
	public install(Vue: any) {
		const socketInstance = io(`${process.env.VUE_APP_PROTOCOL}:${process.env.VUE_APP_API_URL}`, {
			transports: ['websocket'],
		});

		Vue.use(
			new VueSocketIO({
				debug: process.env.NODE_ENV !== 'production',
				connection: socketInstance,
			}),
		);
	}
}

export default new AppSocket();
