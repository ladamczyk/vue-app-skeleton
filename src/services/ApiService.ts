import AbstractService from './AbstractService';

export default class ApiService extends AbstractService {
	constructor() {
		super();

		this.client.defaults.baseURL += '/api';
	}
}
