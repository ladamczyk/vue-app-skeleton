import Axios, { AxiosInstance, AxiosError, AxiosRequestConfig } from 'axios';
import isUndefined from 'lodash/isUndefined';
import { StoreGetters } from '@/store/constants';
import $store from '@/store';
import $wait from '@/wait';
import ApiException from '@/exceptions/ApiException';

export default abstract class AbstractService {
	private _client: AxiosInstance;

	private _request: AxiosRequestConfig;

	get client(): AxiosInstance {
		return this._client;
	}

	set client(value: AxiosInstance) {
		this._client = value;
	}

	get request(): AxiosRequestConfig {
		return this._request;
	}

	set request(value: AxiosRequestConfig) {
		this._request = value;
	}

	// as You can see this config is for token based authorization, should be overridden by You ;)
	protected constructor() {
		const headers = {
			Authorization: `Bearer ${$store.getters[StoreGetters.GET_API_TOKEN]}`,
			'Content-Type': 'application/json',
		};

		this.client = Axios.create({
			baseURL: `${process.env.VUE_APP_PROTOCOL}:${process.env.VUE_APP_API_URL}`,
			headers,
		});

		this.client.interceptors.request.use((config) => {
			this.request = config;
			$wait.start(`loading.service.${config.url}`);

			return config;
		});

		this.client.interceptors.response.use(
			(response) => {
				$wait.end(`loading.service.${this.request.url}`);

				return response;
			},
			(error: AxiosError) => {
				let exception = new ApiException({
					status: 500,
					statusText: 'Network error',
				});

				if (!isUndefined(error.response)) {
					exception = new ApiException(error.response);
				}

				$wait.end(`loading.service.${this.request.url}`);

				return Promise.reject(exception);
			},
		);
	}
}
