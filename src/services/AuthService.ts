import AbstractService from '@/services/AbstractService';
import { CredentialsInterface, UserAuthInterface, UserInterface } from '@/types/services/Auth';
import { AxiosPromise } from 'axios';

export default class AuthService extends AbstractService {
	constructor() {
		super();

		this.client.defaults.baseURL += '/api/auth';
	}

	public check(): AxiosPromise<UserInterface> {
		return this.client.get('/check');
	}

	public login(credentials: CredentialsInterface): AxiosPromise<UserAuthInterface> {
		return this.client.post('/login', credentials);
	}
}
