export interface CredentialsInterface {
	username: string;
	password: string;
}

export interface UserInterface {
	id: number;
	username: string;
}

export interface UserAuthInterface extends UserInterface {
	access_token: string;
}
