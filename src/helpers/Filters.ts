import format from 'date-fns/format';
import fromUnixTime from 'date-fns/fromUnixTime';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';

export const formatDateFromUnix = (value: number, formatString = 'yyyy-MM-dd HH:mm'): string => format(fromUnixTime(value), formatString);
export const formatDateFromString = (value: string, formatString = 'yyyy-MM-dd HH:mm'): string => format(new Date(value), formatString);
export const lastChangedUnix = (value: number): string =>
	formatDistanceToNow(fromUnixTime(value), {
		addSuffix: true,
	});
