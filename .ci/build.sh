#!/bin/bash

BUILD_MESSAGE=$(yarn build)
echo $BUILD_MESSAGE

case "$BUILD_MESSAGE" in
  *"directory is ready to be deployed"*)
    exit 0
    ;;
  *)
    echo "$BUILD_MESSAGE";
    echo 'Fix build errors before commit!';
    exit 1
    ;;
esac
